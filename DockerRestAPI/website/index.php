<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
		<h1>List All - listAll</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll');
            $all = json_decode($json);
            foreach ($all as $item) {
				echo "<li>$item->open_times - $item->close_times</li>";
            }
            ?>
        </ul>
		<h1>List OpenOnly - listOpenOnly</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly');
            $all = json_decode($json);
            foreach ($all as $item) {
				echo "<li>$item->open_times</li>";
            }
            ?>
        </ul>
		<h1>List CloseOnly - listCloseOnly</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly');
            $all = json_decode($json);
            foreach ($all as $item) {
				echo "<li>$item->close_times</li>";
            }
            ?>
        </ul>
		<h1>List All Json Top 5 - listAll/json?top=5</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll/json?top=5');
            $all = json_decode($json);
            foreach ($all as $item) {
				echo "<li>$item->open_times - $item->close_times</li>";
            }
            ?>
        </ul>
		<h1>List OpenOnly Json Top 5 - listOpenOnly/json?top=5</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly/json?top=5');
            $all = json_decode($json);
            foreach ($all as $item) {
				echo "<li>$item->open_times</li>";
            }
            ?>
        </ul>
		<h1>List CloseOnly Json Top 5 - listCloseOnly/json?top=5</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly/json?top=5');
            $all = json_decode($json);
            foreach ($all as $item) {
				echo "<li>$item->close_times</li>";
            }
            ?>
        </ul>
    </body>
</html>
