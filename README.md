Author: Vu Vo
ID: 951437454
Email: vuv@uoregon.edu

# Project 6: Brevet time calculator service

Simple listing service from project 5 stored in MongoDB database.

Usage:
From docker terminal: 
		$ docker-compose up 		OR 		$docker-compose up --build

Note: 	- I did not do csv format so this service will not return csv file!
			- <ipaddress> will be the default IP inside the docker machine. For my Docker for Windows, the IP is 192.168.99.100. To get the correct <ipaddress> of your docker, type the following command from your Docker Terminal:
						$ docker-machine ip default
			
* "http://<ipaddress>:5001/index" will serve a prompt page with the following functionalities:
1) Two buttons ("Submit") and ("Display") in the page where you have controle times. 
2) On clicking the Submit button, the control times were be entered into the database. 

* "http://<ipaddress>:5001/listAll" will return  all open and close times in the database
* "http://<ipaddress>:5001/listOpenOnly" should return open times only
* "http://<ipaddress:5001/listCloseOnly" should return close times only

* "http://<ipaddress>:5001/listAll/json" should return all open and close times in JSON format
* "http://<ipaddress>:5001/listOpenOnly/json" should return open times only in JSON format
* "http://<ipaddress>:5001/listCloseOnly/json" should return close times only in JSON format

* There will also be a query parameter to get top "k" open and close times. For examples, 
    * "http://<ipaddress>:5001/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
    * "http://<ipaddress>:5001/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format

* The URI for my consumer program is as followed:
	* "http://<ipaddress>:5000"

